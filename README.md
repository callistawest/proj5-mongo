# Project 5: Brevet time calculator with Ajax and MongoDB

Reimplement the RUSA ACP controle time calculator with flask and ajax.

Credits to Michal Young for the initial version of this code.

## Background

This program focuses on brevets, calculating accurate brevet times for a race dependent on calculations from https://rusa.org/pages/acp-brevet-control-times-calculator.

For each category of distance, there is a top speed and bottom speed. As the distance of the brevet increases, both the top and bottom speed decrease.

Top speed expected: 0-200km= 34 km/hr 200-400km = 32 km/hr 400-600km = 30 km/hr 600-1000km = 28km/hr

Bottom Speed expected: 0-200km = 15km/hr 200-400km = 15km/hr 400-600 = 11.4 km/hr 600-1000 = 13.3 km/hr

When running the command, "docker-build up", the server will run on https://0.0.0.0:5000. On the ACP Controle Times webpage, the distance, time and date can be control at the top of the page. Additionally, adding milage to the miles category will produce the correct km value, the open time for the brevet as well as the close time of the brevet.

These categories of the table will be filled in automatically after pressing enter for the miles category.

This program builds off project 4, but uses MongoDB.

## Testing

1. Upon running the webpage using localhost:5000, starting on 1/1/2017 at 12am, inputting the miles 20, the open time will be 1/1 0:56, while the close time will be 1/1 2:08. In the next catergory, inputting 40 miles, the open time will be 1/1 1:53 and the close time will be 1/1 4:17. The third brevet with distance 80 will create an open time of 1/1 3:47 and a close time of 1/1 8:34.

2. When these above values have been entered into the table, pressing submit underneith the table will submit all of the information to the display page. The opening page will appear blank after the submit button has been pressed.

3. When a negative number is inputed, an error page (negative.html) appears to alert users to use number equal to 0 or larger.

4. When inputting a distance greater than the distace selected for the race, an error will occur to let the user know to only submit values 20% greater than the distance or smaller. 

5. When pressing submit or display before any information is entered into the table or before any informtaion has been submitted, the error.html page will appear, telling the user to input one or more control time before submitting.

## Credentials

Callista West

CIS 322 Fall 2020

cwest10@uoregon.edu
